const span = document.querySelector('.span');
const intro = document.querySelector('.grid__intro');
const nav = document.querySelector('.menu__nav');
const wrap = document.querySelector('.wrapper');
const noscroll = document.querySelector('.noscroll');
const project = document.querySelector('.projects');
const btnProject = document.querySelector('.btn-project');
const closeProject = document.querySelector('.close');
const projgrad = document.querySelector('.projectgrad');
const checked = document.querySelectorAll('.circle > img');
const buttonPledge = document.querySelectorAll('.button_pledge');
const borderPledge = document.querySelectorAll('.layout');
const cont = document.querySelectorAll('.cont');
const complete = document.querySelector('.complete');
const finito = document.querySelector('.complete_button_finito');

let showProject = false;
let closeProjects = false;
let showMenu = false;


span.addEventListener('click', toggleMenu);
btnProject.addEventListener('click', openProject);
closeProject.addEventListener('click', cancelProject);
finito.addEventListener('click', gotIt);
closeProject.addEventListener('click', closePledge);

//toggle navigation menu
function toggleMenu(){
    if(!showMenu){
        intro.classList.add('open');
        nav.classList.add('open');
        span.classList.add('open');
        wrap.classList.add('open');
        noscroll.classList.add('open');
        showMenu = true;
    }else{
        intro.classList.remove('open')
        nav.classList.remove('open');
        span.classList.remove('open');
        wrap.classList.remove('open');
        noscroll.classList.remove('open');
        showMenu = false;
    }
}

// open back this project page
function openProject(){
    if(!showProject){
        project.classList.add('openproject');
        projgrad.classList.add('open');
        showProject = true;
    }else{
        showProject = false;
    }
   
}
//cancel back this project page
function cancelProject(){
    if(!closeProjects){
        project.classList.remove('openproject');
        projgrad.classList.remove('open');
        closeProjects = true;
    }else{
        closeProjects = false;

       
    }
}
// close thank message page
function gotIt(){
    projgrad.classList.remove('open');
    complete.classList.remove('open');
}
//event for each circle

for(let i = 0; i < checked.length; i++){
    checked[i].addEventListener('click', function(){
        checked[i].classList.toggle("img");

        //event for buttons on active pledge
    for (let j = 0; j < buttonPledge.length; j++){
        if (checked[i].classList.contains('img') && i == j){
            buttonPledge[j].classList.add('pledge');
        }else{
            buttonPledge[j].classList.remove('pledge');

        }
    } 
        //event for border on active pledge
    for (let k = 0; k < borderPledge.length; k++){
        if(checked[i].classList.contains('img') && k == i){
            borderPledge[k].classList.add('pledge');

        }else{
            borderPledge[k].classList.remove('pledge');
        }
    }


    })
}

//event for pledge continue button
for (let j = 0; j < cont.length; j++){
    cont[j].addEventListener('click', function(){
        complete.classList.add('open');
        project.classList.remove('openproject');
        buttonPledge[j].classList.remove('pledge');
        checked[j].classList.remove('img');
    })
}


 //Removing button pledge when you close projects
 function closePledge(){
     buttonPledge.forEach(item =>{
         if(item.classList.contains('pledge')){
             item.classList.remove('pledge');
         }
     })
     checked.forEach(item => {
         if(item.classList.contains('img')){
             item.classList.remove('img');
         }
     })
 }


